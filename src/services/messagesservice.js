import Api from '@/services/api'

export default {
  fetchMessages () {
    return Api().get('/messages')
  },
  postMessage (message) {
    return Api().post('/messages', message,
      { headers: {'Content-type': 'application/json'} })
  }
}
