const apiURL = 'http://localhost:3000/';

describe("add message page", () => {
  beforeEach(() => {
    cy.fixture("message").then(messages => {
      let [d1] = messages;
      let one = [d1];
      one.forEach(message => {
        cy.request("POST", message);
      });
    });
    cy.request(apiURL)
      .its("body")
      .then(messages => {
        messages.forEach(element => {
          cy.request("DELETE", `${apiURL}${element._id}`);
        });
      });
    cy.fixture("messages").then(messages => {
      let [d1, d2, d3, d4, ...rest] = messages;
      let four = [d1, d2, d3, d4];
      four.forEach(message => {
        cy.request("POST", apiURL, message);
      });
    });
    cy.visit("/");
    // Click Message navbar link
    cy.get(".navbar-nav")
      .eq(0)
      .find(".nav-item")
      .eq(2)
      .click();
  });
  describe("Add a Message", () => {
    describe("With valid attributes", () => {
      it("allows a message to be submitted", () => {
        //  Fill out web form
        cy.get("input[data-test=userid]").type(1);
        cy.get("input[data-test=message]").type("Hello");
        cy.contains("Message recived!").should("not.exist");
        //cy.get(".error").should("not.exist");
        cy.get("button[type=submit]").click();
        //cy.contains("Message recived!").should("exist");
      });
      //broke in pc switch will try get back it soon
      after(() => {
        cy.wait(100)
        // Click Manage Messages
        cy.get(".navbar-nav")
        .eq(0)
        .find(".nav-item")
        .eq(1)
        .click();
        // cy.get("tbody")
        //   .find("tr")
        //   .should("have.length", 5);
      });
    });
    describe("With invalid/blank attributes", () => {
      it("shows error messages until all attributes are ", () => {
        cy.get("button[type=submit]").click();
        cy.contains("Please Fill in the Form Correctly.").should("exist");
      });
      after(() => {
        cy.wait(100)
        // Click Manage Messages
        cy.get(".navbar-nav")
        .eq(0)
        .find(".nav-item")
        .eq(1)
        .click();
        cy.get("tbody")
          .find("tr")
          .should("have.length", 4);
      });
    });
  });
});
